class Rack::Attack
  Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new

  throttle('home', limit: 100, period: 1.hour) do |req|
    if req.path == '/home'
      req.ip
    end
  end
end

Rack::Attack.throttled_response = lambda do |env|
  now = Time.now
  match_data = env['rack.attack.match_data']
  n = (match_data[:period] - now.to_i % match_data[:period]).to_s

  [ 429, {}, ["Rate limit exceeded. Try again in #{n} seconds"] ]
end
