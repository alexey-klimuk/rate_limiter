# ENV

ruby 2.4.1, rails 5.1.4

# Set up

1 go to rate_limiter directory

2 run 'gem install bundler'

3 run 'bundle install'

4 run 'rails s'

5 go to http://localhost:3000/home

# Tests

1 go to rate_limiter directory

2 run 'rspec spec/'